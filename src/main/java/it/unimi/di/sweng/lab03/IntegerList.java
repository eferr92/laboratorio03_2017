package it.unimi.di.sweng.lab03;

public class IntegerList {
	private IntegerNode head;
	
	@Override
	public String toString() {
		if(head == null)
			return "[]";
		else
			return "[" + head.toString() + "]";
	}

	public void addLast(int value) {
		if(head == null)
			head = new IntegerNode(value);
		else
			head.addLast(value);
	}
}
